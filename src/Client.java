import java.net.*;
import java.io.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Element;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Client {
    // initialize socket, output stream and input reader
    private Socket socket = null;
    private DataOutputStream out = null;
    private BufferedReader in = null;
    private String algorithm = "";

    public class Server implements Comparable<Server>{
        String type, id, state, time, cores, memory, disk;
        public Server(String type, String id, String state, String time, String cores, String memory, String disk){
            this.type = type;
            this.id = id;
            this.state = state;
            this.time = time;
            this.cores = cores;
            this.memory = memory;
            this.disk = disk;
        }
        public String getType() {
            return type;
        }
        public String getId() {
            return id;
        }
        public String getState() {
            return state;
        }
        public String getTime() {
            return time;
        }
        public String getCores() {
            return cores;
        }
        public String getMemory() {
            return memory;
        }
        public String getDis() {
            return disk;
        }

        @Override
        //compare servers number of cores for sorting
        public int compareTo(Server anotherServers) {
            return Integer.parseInt(this.getCores())- Integer.parseInt(anotherServers.getCores());
        }

    }

    // constructor for a client with ip address, port, algorithm type
    public Client(String address, int port, String type) throws IOException, SAXException, ParserConfigurationException {
        algorithm = type;
        // make connection
        try {
            socket = new Socket(address, port);
            // sends output to the socket (server)
            out = new DataOutputStream(socket.getOutputStream());
            // read input from socket (server)
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (UnknownHostException u) {
            System.out.println(u);
        } catch (IOException i) {
            System.out.println(i);
        }
        //simulate simulation protocol
        simulation();
        // close the connection
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    private void simulation() throws ParserConfigurationException, IOException, SAXException { // JVM takes care of exceptions
        //File fXmlFile = new File("/home/comp335/Downloads/system.xml");
        File fXmlFile = new File("system.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        NodeList server = doc.getElementsByTagName("server"); // search for server tag

        String highestType = "";
        int highestCore = 0;
        for (int i = 0; i < server.getLength(); i++) { // loop through each server
            int currentCore = Integer.parseInt(((Element) server.item(i)).getAttribute("coreCount"));
            if (currentCore > highestCore) { // compare each server's core count to find largest server
                highestCore = currentCore;
                highestType = ((Element) server.item(i)).getAttribute("type");
            }
        }

        //initial handshake with server
        out.write(("HELO" + "\n").getBytes());
        out.write(("AUTH comp335" +"\n").getBytes());
        in.skip(3);
        //loop to do the scheduling
        while(true) {
            out.write(("REDY" + "\n").getBytes());
            String readInput;
            readInput = in.readLine();
            //server replies OK
            if (readInput.equals("OK")) {
                readInput = in.readLine();
                //server has jobs to schedule
                if (!readInput.equals("NONE")) {
                    //breaking up each component in job submission information and storing into an array of strings
                    String [] jobN = readInput.split(" ");
                    //requesting information of servers available with sufficient resources of job requirements (CPU cores, memory and disk)
                    out.write(("RESC Avail" + " " + jobN[4] + " " + jobN[5] + " " + jobN[6] + "\n").getBytes());
                    readInput = in.readLine();
                    //sending OK to each job on server
                    if (readInput.equals("DATA")) {
                        out.write(("OK" + "\n").getBytes());
                        readInput = in.readLine();
                        //initialise ArrayList
                        List<Server> serv = new ArrayList<>();
                        //stop once you read "."
                        while (!readInput.equals(".")) {
                            String[] serverInfo = readInput.split(" ");
                            //store each server information into ArrayList
                            serv.add(new Server(serverInfo[0], serverInfo[1], serverInfo[2], serverInfo[3], serverInfo[4], serverInfo[5], serverInfo[6]));
                            out.write(("OK" + "\n").getBytes());
                            readInput = in.readLine();
                        }
                        //sort server ArrayList by number of cores
                        Collections.sort(serv);
                        //first fit algorithm
                        if (algorithm.equals("ff")) {
                            //schedule the job to first server

                            int firstFit = -1;
                            String firstServType = "";
                            String firstServId = "";  
                                         

                            for (int i = 0; i < serv.size(); i++) {
                                int nCores = Integer.parseInt(serv.get(i).getCores()) ;
                                int nJobCores = Integer.parseInt(jobN[4]) ;
                                if (nCores >= nJobCores) {
                                    firstFit = i;
                                    firstServType = serv.get(i).getType();
                                    firstServId = serv.get(i).getId();
                                       break;
                                }

                            }
                            //first fit found, schedule to first fit server
                            if (firstFit != -1) {
                                out.write(("SCHD" + " " + jobN[2] + " " + firstServType + " " + firstServId + "\n").getBytes());
                            }
						//assign to idle server algorithm
						} else if (algorithm.equals("idf")) {
							
								int idleFit = -1;
String idleServType = "";
String idleServId = "";
  
for (int i = 0; i < serv.size(); i++) {

int state = Integer.parseInt(serv.get(i).getState());
//first idle server found
if(state == 2) {
idleFit = i;
break;
}
}
//Idle server existed
if (idleFit != -1) {
                        idleServType = serv.get(idleFit).getType();
                        idleServId = serv.get(idleFit).getId();
out.write(("SCHD" + " " + jobN[2] + " " + idleServType + " " + idleServId + "\n").getBytes());
//No idle server existed use DRF-liked strategies
} else {
//Retrieve CPU and memory info from job queue with initialisation
                                int nJobCores = Integer.parseInt(jobN[4]) ;
                                int nJobMem = Integer.parseInt(jobN[5]) ;
int tolCores = 0;
int tolMemory = 0;

for (int i = 0; i < serv.size(); i++) {
//Retrieve CPU and memory info from server and sum up their number
int nCores = Integer.parseInt(serv.get(i).getCores()) ;
int nMemory = Integer.parseInt(serv.get(i).getMemory()) ;
tolCores = tolCores + nCores;
tolMemory = tolMemory + nMemory;
}
//Total CPU and Memory consumption
double jbPerCores = nJobCores / tolCores;
double jbPerMemory = nJobMem / tolMemory;

                            int drfFit = -1;
String drfServType = "";
String drfServId = "";
//Prepare to schedule the first server with the least CPU difference
for (int i = 0; i < serv.size(); i++) {
int current = Integer.parseInt(serv.get(i).getCores())- Integer.parseInt(jobN[4]);
if ((current < tolCores) || (current == tolCores) ) {
drfServType = serv.get(i).getType();
drfServId = serv.get(i).getId();
drfFit = i;
break;
}
}
out.write(("SCHD" + " " + jobN[2] + " " + drfServType + " " + drfServId + "\n").getBytes());
								
                            //best fit algorithm
                        } else if (algorithm.equals("bf")){

                            int bestFit = Integer.MAX_VALUE;
                            int minAvail = Integer.MAX_VALUE;
                            boolean bfFound = false;
                            String bestServerType = "";
                            String bestServerId = "";
                            for (int i = 0; i < serv.size(); i++) {
                                int current = Integer.parseInt(serv.get(i).getCores()) - Integer.parseInt(jobN[4]) ;
                                int availTime = Integer.parseInt(serv.get(i).getTime());
                                if ((current < bestFit) || (current == bestFit) && (availTime < minAvail)) {
                                    bestFit = current;
                                    minAvail = availTime;
                                    bestServerType = serv.get(i).getType();
                                    bestServerId = serv.get(i).getId();
                                    bfFound = true;
                                }
                            }
                            //best fit found, schedule to best fit server
                            if (bfFound) {
                                out.write(("SCHD" + " " + jobN[2] + " " + bestServerType + " " + bestServerId + "\n").getBytes());
                                //else schedule to best fit active server
                            } else {
                                String bestActiveType = "";
                                String bestActiveId = "";
                                for (int i = 0; i < serv.size(); i++) {
                                    int state = Integer.parseInt(serv.get(i).getState());
                                    //first best fit active server
                                    if(state == 3) {
                                        bestActiveType = serv.get(i).getType();
                                        bestActiveId = serv.get(i).getId();
                                        break;
                                    }
                                }
                                out.write(("SCHD" + " " + jobN[2] + " " + bestActiveType + " " + bestActiveId + "\n").getBytes());
                                System.out.println("HELLO");
                            }
                            //worst fit algorithm
                        } else if (algorithm.equals("wf")){
                            int worstFit = 0;
                            int altFit = 0;
                            boolean wfFound = false;
                            boolean altFound = false;
                            String worstServerType = "";
                            String worstServerId = "";
                            String altServerType = "";
                            String altServerId = "";
                            for (int i = 0; i < serv.size(); i++) {
                                int current = Integer.parseInt(serv.get(i).getCores()) - Integer.parseInt(jobN[4]) ;
                                int time = Integer.parseInt(serv.get(i).getTime());

                                //server availTime = job time
                                if (((current > worstFit) && (time == Integer.parseInt(jobN[1])))) {
                                    worstFit = current;
                                    worstServerType = serv.get(i).getType();
                                    worstServerId = serv.get(i).getId();
                                    wfFound = true;
                                }
                                if ((current > altFit) && (Integer.parseInt(jobN[4]) - Integer.parseInt(serv.get(i).getTime())) != 0) {
                                    altFit = current;
                                    altServerType = serv.get(i).getType();
                                    altServerId = serv.get(i).getId();
                                    altFound = true;
                                }
                                if (current == worstFit) {
                                    worstFit = current;
                                    worstServerType = serv.get(i).getType();
                                    worstServerId = serv.get(i).getId();
                                    wfFound = true;
                                    break;
                                }
                            }

                            //worst fit found, schedule to worst fit server
                            if (wfFound) {
                                out.write(("SCHD" + " " + jobN[2] + " " + worstServerType + " " + worstServerId + "\n").getBytes());
                                //else alt fit found, schedule to alt fit server
                            } else if (altFound) {
                                out.write(("SCHD" + " " + jobN[2] + " " + altServerType + " " + altServerId + "\n").getBytes());
                                //else schedule to worst fit active server
                            } else {
                                String worstActiveType = "";
                                String worstActiveId = "";
                                for (int i = serv.size() - 1; i > serv.size(); i--) {
                                    int state = Integer.parseInt(serv.get(i).getState());
                                    if(state == 3) {
                                        worstActiveType = serv.get(i).getType();
                                        worstActiveId = serv.get(i).getId();
                                        break;
                                    }
                                }
                                out.write(("SCHD" + " " + jobN[2] + " " + worstActiveType + " " + worstActiveId + "\n").getBytes());
                            }
                        } else {
                            //allToLargest default algorithm      
                            //schedule the job to largest server
                            out.write(("SCHD" + " " + jobN[2]  + " " + highestType + " " + "0" + "\n").getBytes());
                        }
                        //repeat for next job
                    }

                } else {
                    //terminate simulation when there is no more jobs
                    out.write(("QUIT" + "\n").getBytes());
                }
            } else {
                //exit if server doesn't send OK
                break;
            }
        }
    }


    public static void main(String args[]) throws IOException, SAXException, ParserConfigurationException {
        //read which algorithm user specifies
        String algorithmType = "";
        for (int i = 0; i<args.length; i++) {
            if (args[i].equals("-a")) {
                if (args.length == i+1) {
                } else {
                    algorithmType = args[i+1]; // "ff" "wf" "bf"
                }
                break;
            }
        }
        Client client = new Client("127.0.0.1", 8096, algorithmType);
    }

    //server class to store server information


}